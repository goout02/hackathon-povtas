class InvalidAPIUsage(Exception):
    status_code = 400

    def __init__(self, message: str, status_code: int = None , payload=None):
        super().__init__()

        self.message = message
        if status_code is not None:
            self.status_code = status_code

        self.payload = payload

    def to_dict(self):
        rv = {
            'payload': dict(self.payload or ()), 
            'message': self.message
        }
        return rv
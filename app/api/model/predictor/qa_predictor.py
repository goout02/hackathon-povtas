import torch
from gensim.models import Doc2Vec

from .tokenizer import Tokenizer
from .text_preparator import TextPreparator
from .classify_model import QAClassifierModel


class QAPredictor:
    def __init__(self, model_path: str, doc2vec_model_path: str, input_size: int, tokenizer: Tokenizer, preparator: TextPreparator):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = QAClassifierModel(input_size).to(self.device)
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()

        self.doc2vec_model: Doc2Vec = Doc2Vec.load(doc2vec_model_path)
        self.tokenizer = tokenizer
        self.preparator = preparator

    def predict(self, question, answer):
        question_tokens = self.tokenizer(self.preparator(question))
        answer_tokens = self.tokenizer(self.preparator(answer))

        question_embedding = self.doc2vec_model.infer_vector(question_tokens)
        answer_embedding = self.doc2vec_model.infer_vector(answer_tokens)
        # print(question_tokens)
        # print(answer_tokens)
        with torch.no_grad():
            question_tensor = torch.tensor(question_embedding, dtype=torch.float32).to(self.device)
            answer_tensor = torch.tensor(answer_embedding, dtype=torch.float32).to(self.device)
            output = self.model(question_tensor, answer_tensor, dim=0)

            prediction = output.squeeze().item()
        # print(prediction)
        result = int(prediction * 10)
        return  result if result != 0 else 1

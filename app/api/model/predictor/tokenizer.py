from spacy.lang.ru import Russian
from typing import List


class Tokenizer(object):
    def __init__(self):
        self.nlp = Russian()

    def tokenize(self, text: str) -> List[str]:
        return [token.text for token in self.nlp(text)]
    
    def __call__(self, text: str) -> List[str]:
        return self.tokenize(text)
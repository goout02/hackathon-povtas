import nltk
import re
import pymorphy2
from nltk.corpus import stopwords
from typing import List
nltk.download('stopwords')


class TextPreparator(object):
    def __init__(self):
        self.stop_words = set(stopwords.words('russian'))

        self.morph = pymorphy2.MorphAnalyzer()
    
    def __lower(self, text: str) -> str:
        '''Function convert text to lower case'''
        return text.lower()
    
    def __get_lemmas(self, words: List[str]) -> List[str]:
        '''Function do lemmatize for every word in input list'''
        return [self.morph.parse(word)[0].normal_form for word in words]
            
    def __remove_stopwords(self, words: List[str]) -> List[str]:
        '''
        Function remove stop words from a list of words
            Args:
                words (List[str]): List of words
            Returns:
                List of words
        '''
        return [word for word in words if word not in self.stop_words]
    
    def __filter_words_by_length(self, words: List[str], length: int) -> List[str]:
        '''
        Function filter words by length greater then lenght param
            Args:
                words (List[str]): List of words
                
                length (int): Needed length to filter
            Returns:
                List of words greater then length param
        '''
        return [word for word in words if len(word) > length]
    
    def prepare_text(self, text: str) -> str:
        text = self.__lower(text)
        words = re.findall(r'[а-яА-Я]+', text)
        words = self.__remove_stopwords(words)
        words = self.__get_lemmas(words)
        words = self.__filter_words_by_length(words, 2)

        return ' '.join(words)
    
    def __call__(self, text: str) -> str:
        return self.prepare_text(text)

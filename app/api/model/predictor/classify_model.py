import torch
import torch.nn as nn


class QAClassifierModel(nn.Module):
    def __init__(self, input_size):
        super(QAClassifierModel, self).__init__()
        self.fc1 = nn.Linear(input_size * 2, 256)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(256, 256)
        self.relu1 = nn.ReLU()
        self.fc3 = nn.Linear(256, 1)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x1, x2, dim=1):
        x = torch.cat([x1, x2], dim=dim)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.relu1(x)
        x = self.fc3(x)
        x = self.sigmoid(x)
        return x
    
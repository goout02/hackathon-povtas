from api_error import InvalidAPIUsage

def parse_json(json: dict):
    params = {
        'student_id': json.get('student_id' ),
        'question_id' : json.get('question_id' ),
        'answer': json.get('answer')
    }

    if not (params['student_id'] and params['question_id'] and params['answer'] ): 
        raise InvalidAPIUsage('Bad Payload', payload=json)
    
    return params
    
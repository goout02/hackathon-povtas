import json

from api_error import InvalidAPIUsage
from constance.questions import PATH_TO_QUESTIONS

from model.predictor import QAPredictor, Tokenizer, TextPreparator
from constance.predictor import MODEL_PATH, DOC2VEC_MODEL_PATH, INPUT_SIZE

class Answer:
    answer = None
    student_id = None
    question_id = None
    evaluation = None

    def __init__(self, json: dict):
        self.create(json)

        tokenizer = Tokenizer()
        text_preparator = TextPreparator()

        self.predictor = QAPredictor(
            model_path=MODEL_PATH, 
            doc2vec_model_path=DOC2VEC_MODEL_PATH, 
            input_size=INPUT_SIZE, 
            tokenizer=tokenizer, 
            preparator=text_preparator
        )
        
    def create(self, json: dict):
        self.answer = json['answer']
        self.student_id = json['student_id']
        self.question_id= json['question_id']

    def evaluate(self):
        with open(PATH_TO_QUESTIONS, 'r', encoding='utf-8') as file:
            questions = json.load(file)

        question_text = questions[self.question_id]
        
        self.evaluation = self.predictor.predict(question_text, self.answer)

    def to_dict(self): 
        result = {
            'answer': self.answer,
            'student_id': self.student_id,
            'question_id': self.question_id,
        }

        if (self.evaluation):
            result.update({'evaluation': self.evaluation}),

        return result
            

    
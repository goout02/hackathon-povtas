from flask import Flask, request, json, jsonify, abort

from api_error import InvalidAPIUsage
from model.answer.Answer import Answer
from model.answer.parse_json import parse_json


app = Flask(__name__)

@app.errorhandler(InvalidAPIUsage)
def invalid_api_usage(e):
    return jsonify(e.to_dict()), e.status_code

@app.route('/', methods=['GET'])
def index():
    return 'Hello world'


@app.route('/api/predict', methods=['POST'])
def predict():
    answer = Answer(parse_json(request.json))
    answer.evaluate()
    return json.dumps(
        answer.to_dict(),
        ensure_ascii=False,
    )
    

if __name__ == '__main__':
    import os
    print(os.getcwd())
    app.run(debug=False, port=5050, host='0.0.0.0')
    app.register_error_handler(400, invalid_api_usage)



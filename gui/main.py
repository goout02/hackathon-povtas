import flet as ft
import os
import json
import time
import requests
from typing import List, Dict

DEFAULT_FLET_PORT = 8502
ID_LENGTH = 8
ID_TO_DATA = {'B': 'BSTU', 'DS': 'DataScience'}
DATA_TO_ID = {'BSTU': 'B', 'DataScience': 'DS'}
API_URL = "http://flask_app:5050"


def get_question_list() -> List:
    with open("data/gui_data.json", 'r', encoding="UTF-8") as file:
        gui_data = json.load(file)
        return gui_data


def get_info_dict() -> Dict:
        gui_data = get_question_list()
        id_list = [line['id'] for line in gui_data]
        info_dict = {}

        for index in id_list:
            info = index.split('-')
            if ID_TO_DATA[info[0]] not in info_dict:
                info_dict.update({ID_TO_DATA[info[0]]: {}})
            if ID_TO_DATA[info[1]] not in info_dict[ID_TO_DATA[info[0]]]:
                info_dict[ID_TO_DATA[info[0]]].update({ID_TO_DATA[info[1]]: {}})
            if info[2] not in info_dict[ID_TO_DATA[info[0]]][ID_TO_DATA[info[1]]]:
                info_dict[ID_TO_DATA[info[0]]][ID_TO_DATA[info[1]]].update({info[2]: []})
            if info[3] not in info_dict[ID_TO_DATA[info[0]]][ID_TO_DATA[info[1]]][info[2]]:
                info_dict[ID_TO_DATA[info[0]]][ID_TO_DATA[info[1]]][info[2]].append(info[3])
        return info_dict


QUESTIONS = get_question_list()
DATA = get_info_dict()


def main(page: ft.Page) -> None:
    page.theme_mode = ft.ThemeMode.DARK
    page.title = 'Русы NLP'

    output_info = {
        'student_id': '',
        'question_id': '',
        'answer': ''}

    student_id = ft.TextField(
        label="Enter your student id",
        max_length=ID_LENGTH,
        input_filter=ft.NumbersOnlyInputFilter(),
        width=300,
        border_radius=20,
        text_align=ft.TextAlign.CENTER)

    question_text = ft.TextField(
        label='Question',
        read_only=True,
        multiline=True)

    question_text_res = ft.TextField(
        label='Question',
        read_only=True,
        multiline=True)

    def get_question(q_id: str) -> str:
        for index in QUESTIONS:
            if q_id == index['id']:
                return index['question']

    def convert_to_index() -> str:
        return DATA_TO_ID[institute.value] + '-' + DATA_TO_ID[
            discipline.value] + '-' + topic.value + '-' + question.value

    def get_question_page(e):
        q_id = convert_to_index()
        output_info.update({'question_id': q_id})
        question_text.value = get_question(q_id)
        page.go("/main/question")

    def get_predict():
        response = requests.post(API_URL + '/api/predict', json=output_info)
        if response.status_code != 200:
            return

        result = response.json()
        evaluation = result.get('evaluation')

        return evaluation

    def send_answer(e):
        output_info.update({'answer': answer.value})
        answer_resulted.value = answer.value
        question_text_res.value = question_text.value

        with open(f"media/responses/{time.strftime('%H%M%S-%d%m%y', time.localtime())}.json", "a+", encoding="UTF-8") as outfile:
            json.dump(output_info, outfile, ensure_ascii=False)

        evaluation_value = get_predict()
        if not evaluation_value:
            return

        evaluation.value = evaluation_value
        output_info.update({'evaluation': evaluation.value})
        page.go("/main/question/result")

        with open(f"media/responses/res-{time.strftime('%H%M%S-%d%m%y', time.localtime())}.json", "a+", encoding="UTF-8") as outfile:
            json.dump(output_info, outfile, ensure_ascii=False)

    send_button = ft.ElevatedButton(
        disabled=True,
        text='Send',
        color='white',
        bgcolor='#1a1c1e',
        on_click=send_answer
    )

    submit_button = ft.ElevatedButton(
        disabled=True,
        text='Submit',
        color='white',
        bgcolor='#1a1c1e',
        on_click=get_question_page
    )

    def question_selected(e) -> None:
        submit_button.disabled = False
        page.update()

    question = ft.Dropdown(
        disabled=True,
        label="question",
        on_change=question_selected,
        hint_text="Choose your question",
        width=450,
    )

    def topic_selected(e) -> None:
        question.disabled = False
        submit_button.disabled = True
        question.options = [ft.dropdown.Option(name) for name in DATA[institute.value][discipline.value][topic.value]]
        page.update()

    topic = ft.Dropdown(
        disabled=True,
        label="topic",
        on_change=topic_selected,
        hint_text="Choose your topic",
        width=450,
    )

    def discipline_selected(e) -> None:
        topic.disabled = False
        question.disabled = True
        submit_button.disabled = True
        topic.options = [ft.dropdown.Option(name) for name in DATA[institute.value][discipline.value].keys()]
        page.update()

    discipline = ft.Dropdown(
        disabled=True,
        on_change=discipline_selected,
        label="discipline",
        hint_text="Choose your discipline",
        width=450,
    )

    def institute_selected(e) -> None:
        discipline.disabled = False
        topic.disabled = True
        question.disabled = True
        submit_button.disabled = True
        discipline.options = [ft.dropdown.Option(name) for name in DATA[institute.value].keys()]
        page.update()

    institute = ft.Dropdown(
        label="Institute",
        on_change=institute_selected,
        hint_text="Choose your institute",
        options=[ft.dropdown.Option(name) for name in DATA.keys()],
        width=450,
    )

    def auth(e) -> None:
        if len(student_id.value) == ID_LENGTH:
            output_info.update({'student_id': student_id.value})
            page.go("/main")
        else:
            student_id.error_text = "Incorrect student id"
            page.update()

    def answer_to_send(e) -> None:
        if len(answer.value) > 0:
            send_button.disabled = False
        else:
            send_button.disabled = True
        page.update()

    answer = ft.TextField(
        label='Enter your answer',
        on_change=answer_to_send,
        min_lines=5,
        multiline=True)

    answer_resulted = ft.TextField(
        label='Answer',
        read_only=True,
        min_lines=5,
        multiline=True)

    evaluation = ft.TextField(
        label='evaluation',
        value='~value',
        read_only=True)

    def get_page_from_json(path: str) -> None:
        with open(path, 'r', encoding="UTF-8") as file:
            input_data = json.load(file)
            if ('student_id' and 'question_id' and 'answer') in input_data.keys():
                student_id.value = input_data['student_id']
                question_text_res.value = get_question(input_data['question_id'])
                answer_resulted.value = input_data['answer']
                output_info.update({'student_id': student_id.value, 'question_id': input_data['question_id'], 'answer': answer_resulted.value})

                evaluation_value = get_predict()
                if not evaluation_value:
                    return

                evaluation.value = evaluation_value
                output_info.update({'evaluation': evaluation.value})
                page.go("/main/question/result")

                with open(f"media/responses/{my_pick.result.files[-1].name.split('.')[0]}-res-{time.strftime('%H%M%S-%d%m%y', time.localtime())}.json", "a+",
                          encoding="UTF-8") as outfile:
                    json.dump(output_info, outfile, ensure_ascii=False)

    def get_json(e: ft.FilePickerResultEvent) -> None:
        print(my_pick.result.files[-1].name)
        uf = ft.FilePickerUploadFile(
            my_pick.result.files[-1].name,
            upload_url=page.get_upload_url(my_pick.result.files[-1].name, 600),
        )
        my_pick.upload([uf])
        time.sleep(0.1)
        path = f"media/uploads/{my_pick.result.files[-1].name.split('.')[0] + time.strftime('-%H%M%S-%d%m%y', time.localtime())}.json"
        os.rename(f"media/uploads/{my_pick.result.files[-1].name}", path)
        get_page_from_json(path)

    my_pick = ft.FilePicker(on_result=get_json)
    page.overlay.append(my_pick)

    def route_change(route) -> None:
        page.views.clear()
        page.views.append(
            ft.View(
                "/",
                [
                    ft.Container(
                                  content=ft.Column([
                                      ft.Text(
                                          value='Welcome',
                                          font_family='Dhurjati',
                                          size=30,
                                          width=450,
                                          text_align=ft.TextAlign.CENTER),
                                      ft.Container(
                                          content=student_id,
                                          alignment=ft.alignment.center
                                      ),
                                      ft.Container(
                                          content=ft.ElevatedButton(
                                              text='Log in',
                                              color='white',
                                              bgcolor='#1a1c1e',
                                              on_click=auth,
                                          ),
                                          alignment=ft.alignment.center

                                      )]),
                                  width=450,
                                  bgcolor='#515151',
                                  border_radius=20,
                                  alignment=ft.alignment.center,
                                  padding=ft.padding.only(top=65, bottom=65, right=30, left=30),
                                  shadow=ft.BoxShadow(
                                      spread_radius=1,
                                      blur_radius=15,
                                      color=ft.colors.BLACK,
                                      offset=ft.Offset(0, 0),
                                      blur_style=ft.ShadowBlurStyle.OUTER,
                                  )
                              ),
                    ft.Text(value='or', size=30),
                    ft.Container(
                        content=ft.Container(
                                content=ft.ElevatedButton(
                                    text='Upload .json file',
                                    color='white',
                                    bgcolor='#1a1c1e',
                                    on_click=lambda _: my_pick.pick_files(initial_directory='../data/gui_data.json', allowed_extensions=['json']),
                                )
                        ),
                        width=450,
                        bgcolor='#515151',
                        border_radius=20,
                        alignment=ft.alignment.center,
                        padding=ft.padding.only(top=30, bottom=30, right=30, left=30),
                        shadow=ft.BoxShadow(
                            spread_radius=1,
                            blur_radius=15,
                            color=ft.colors.BLACK,
                            offset=ft.Offset(0, 0),
                            blur_style=ft.ShadowBlurStyle.OUTER,
                        )
                    )
                ],
                vertical_alignment=ft.MainAxisAlignment.CENTER,
                horizontal_alignment=ft.CrossAxisAlignment.CENTER,
            )
        )
        if page.route == "/main" or page.route == "/main/question" or page.route == "/main/question/result":
            page.views.append(
                ft.View(
                    "/main",
                    [
                        ft.AppBar(title=ft.Text(f"student id: {student_id.value}",), bgcolor='#515151', center_title=True),
                        ft.Container(
                            content=ft.Column([institute,
                                               discipline,
                                               topic,
                                               question,
                                               ft.Container(
                                                   content=submit_button,
                                                   alignment=ft.alignment.center)
                                               ]),
                            width=450,
                            bgcolor='#515151',
                            border_radius=20,
                            alignment=ft.alignment.center,
                            padding=ft.padding.only(top=65, bottom=65, right=30, left=30),
                            shadow=ft.BoxShadow(
                                spread_radius=1,
                                blur_radius=15,
                                color=ft.colors.BLACK,
                                offset=ft.Offset(0, 0),
                                blur_style=ft.ShadowBlurStyle.OUTER,
                            )
                        )
                    ],
                    vertical_alignment=ft.MainAxisAlignment.CENTER,
                    horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                )
            )
            if page.route == "/main/question" or page.route == "/main/question/result":
                page.views.append(
                    ft.View(
                        "/main/question",
                        [
                            ft.AppBar(title=ft.Text(f"student id: {student_id.value}", ), bgcolor='#515151',
                                      center_title=True),
                            ft.Container(
                                content=ft.Column([
                                    question_text,
                                    answer,
                                    ft.Container(
                                        content=send_button,
                                        alignment=ft.alignment.center)],
                                    spacing=20),
                                width=800,
                                bgcolor='#515151',
                                border_radius=20,
                                alignment=ft.alignment.center,
                                padding=ft.padding.only(top=65, bottom=65, right=30, left=30),
                                shadow=ft.BoxShadow(
                                    spread_radius=1,
                                    blur_radius=15,
                                    color=ft.colors.BLACK,
                                    offset=ft.Offset(0, 0),
                                    blur_style=ft.ShadowBlurStyle.OUTER,
                                )
                            )
                        ],
                        vertical_alignment=ft.MainAxisAlignment.CENTER,
                        horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                    )

                )
                if page.route == "/main/question/result":
                    page.views.append(
                        ft.View(
                            "/main/question/result",
                            [
                                ft.AppBar(title=ft.Text(f"student id: {student_id.value}", ), bgcolor='#515151',
                                          center_title=True),
                                ft.Container(
                                    content=ft.Column([
                                        question_text_res,
                                        answer_resulted,
                                        evaluation,
                                        ft.Container(
                                            content=ft.ElevatedButton(
                                                text='Go Main page',
                                                color='white',
                                                bgcolor='#1a1c1e',
                                                on_click=lambda _: page.go('/'),
                                            ),
                                            alignment=ft.alignment.center

                                        )
                                    ],
                                        spacing=20),
                                    width=800,
                                    bgcolor='#515151',
                                    border_radius=20,
                                    alignment=ft.alignment.center,
                                    padding=ft.padding.only(top=65, bottom=65, right=30, left=30),
                                    shadow=ft.BoxShadow(
                                        spread_radius=1,
                                        blur_radius=15,
                                        color=ft.colors.BLACK,
                                        offset=ft.Offset(0, 0),
                                        blur_style=ft.ShadowBlurStyle.OUTER,
                                    )
                                )
                            ],
                            vertical_alignment=ft.MainAxisAlignment.CENTER,
                            horizontal_alignment=ft.CrossAxisAlignment.CENTER,
                        )

                    )
        page.update()

    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    page.on_route_change = route_change
    page.on_view_pop = view_pop
    page.go(page.route)


if __name__ == '__main__':
    flet_port = int(os.getenv("FLET_PORT", DEFAULT_FLET_PORT))
    ft.app(target=main, upload_dir="media/uploads", view=ft.AppView.WEB_BROWSER, port=flet_port)

